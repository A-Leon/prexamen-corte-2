package com.example.sistemadb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.example.sistemadb.database.AgendaProductos;
import com.example.sistemadb.database.Productos;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText edtNombre;
    private EditText edtMarca;
    private EditText edtPrecio;
    private EditText edtCodigo;

    private RadioGroup rg;
    private RadioButton cbxCaducidad;
    private AgendaProductos db;
    private Productos savedProductos;
    private long id;
    private RadioButton rb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre= (EditText) findViewById(R.id.txtNombre);
        edtMarca= (EditText) findViewById(R.id.txtMarca);
        edtPrecio= (EditText) findViewById(R.id.txtPrecio);
        edtCodigo= (EditText) findViewById(R.id.txtCodigo);
        rb =(RadioButton)  findViewById(R.id.r1);

        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        rg= (RadioGroup) findViewById(R.id.lblradiog) ;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnEditar = (Button) findViewById(R.id.btnEditar);
        db = new AgendaProductos(MainActivity.this);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtNombre.getText().toString().equals("")||
                        edtCodigo.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Inserte código y nombre de Producto",
                            Toast.LENGTH_SHORT).show();
                }else{

                    Productos nProductos = new Productos();
                    nProductos.setNombre(edtNombre.getText().toString());
                    nProductos.setCodigo(edtCodigo.getText().toString());
                    nProductos.setMarca(edtMarca.getText().toString());

                    int radiocheck =rg.getCheckedRadioButtonId();
                    if( radiocheck==2131165286){
                        nProductos.setCaducidad("Perecedero");

                    }
                    if( radiocheck==2131165287){
                        nProductos.setCaducidad(" No Perecedero");

                    }

                    db.openDataBase();
                    if(savedProductos== null){
                        long idx= db.insertarProducto(nProductos);
                        Toast.makeText(MainActivity.this, "Se agregó Producto de Código: " + idx,Toast.LENGTH_SHORT).show();
                    }



                }
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ProductoActivity.class);

                startActivityForResult(i,0);

            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });





    }

    public void limpiar()
    {
        edtNombre.setText("");
        edtCodigo.setText("");
        edtMarca.setText("");
        edtPrecio.setText("");
        rb.setChecked(true);



    }




}
