package com.example.sistemadb.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaProductos {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;

    private String [] columnsToRead = new String[]{
            DefinirTabla.Producto.CODIGO,
            DefinirTabla.Producto.NOMBRE_PRODUCTO,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.PERECEDERO

    };
    public AgendaProductos(Context context){
        this.context = context;
        agendaDbHelper = new AgendaDbHelper(this.context);
    }

    public void openDataBase(){
        db = agendaDbHelper.getWritableDatabase();
    }

    public long insertarProducto(Productos  c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE_PRODUCTO,c.getNombre());
        values.put(DefinirTabla.Producto.MARCA,c.getPrecio());
        values.put(DefinirTabla.Producto.PRECIO,c.getMarca());
        values.put(DefinirTabla.Producto.PERECEDERO,c.getCaducidad());
        return db.insert(DefinirTabla.Producto.TABLE_PRODUCTO, null,values);


    }

    public long UpdateProducto (Productos c, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE_PRODUCTO,c.getNombre());
        values.put(DefinirTabla.Producto.MARCA,c.getPrecio());
        values.put(DefinirTabla.Producto.PRECIO,c.getMarca());
        values.put(DefinirTabla.Producto.PERECEDERO,c.getCaducidad());
        values.put(DefinirTabla.Producto.CODIGO,c.getCodigo());
        String criterio = DefinirTabla.Producto.ID + " = " +id;
        return db.update(DefinirTabla.Producto.TABLE_PRODUCTO, values, criterio, null);

    }

    public int deleteProducto(long id){
        String criterio =  DefinirTabla.Producto.CODIGO+ " = " + id;
        return db.delete(DefinirTabla.Producto.TABLE_PRODUCTO, criterio, null);
    }

    public Productos readProductos(Cursor cursor){
        Productos c = new Productos();
        c.setId(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setMarca(cursor.getString(2));
        c.setPrecio(cursor.getString(3));
        c.setCodigo(cursor.getString(4));
        c.setCaducidad(cursor.getString(5));
        return c;
    }

    public Productos getContacto(long id){
        SQLiteDatabase db = agendaDbHelper.getWritableDatabase();
        Cursor c= db.query(DefinirTabla.Producto.TABLE_PRODUCTO, columnsToRead,
                DefinirTabla.Producto.CODIGO+ " = ?",
                new String[]{String.valueOf(id)},
                null, null,null);
        c.moveToFirst();
        Productos productos = readProductos(c);
        c.close();
        return productos;
    }

    public ArrayList<Productos> allContactos(){

        ArrayList<Productos> productos = new  ArrayList<Productos>();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLE_PRODUCTO,null, null,
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Productos c= readProductos(cursor);
            productos.add(c);
            cursor.moveToNext();

        }
        cursor.close();
        return productos;

    }

    public void cerrar(){
        agendaDbHelper.close();
    }
}