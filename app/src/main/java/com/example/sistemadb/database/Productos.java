package com.example.sistemadb.database;

import java.io.Serializable;

public class Productos implements Serializable {
    private String codigo;
    private String nombre;
    private String Marca;
    private String precio;
    private String caducidad;
    private long id;

    public Productos( long id, String codigo, String nombre, String marca, String precio, String caducidad) {
        this.id= id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.Marca = marca;
        this.precio = precio;
        this.caducidad = caducidad;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Productos() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCaducidad() {
        return caducidad;
    }

    public void setCaducidad(String caducidad) {
        this.caducidad = caducidad;
    }
}
