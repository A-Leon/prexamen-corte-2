package com.example.sistemadb.database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla() {
    }

    public static abstract class Producto implements BaseColumns {

        public final static String ID = "id";
        public final static String CODIGO = "codigo";
        public final static String NOMBRE_PRODUCTO = "nombre";
        public final static String MARCA = "marca";
        public final static String PRECIO = "precio";
        public final static String PERECEDERO = "perecedero";
        public final static String TABLE_PRODUCTO = "producto";

    }
}