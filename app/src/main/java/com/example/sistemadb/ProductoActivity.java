package com.example.sistemadb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sistemadb.database.AgendaProductos;
import com.example.sistemadb.database.Productos;

public class ProductoActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private TextView lbltel2;
    private EditText txtNombre;
    private TextView lblMarca;
    private EditText txtMarca;
    private TextView lblNota;
    private EditText txtPrecio;
    private RadioGroup lblradiog;

    private Button btnBorrar;
    private Button btnEditar;
    private Button btnNuevo;
    private Button btnBuscar;
    private AgendaProductos db;
    private Productos saveProducto = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        txtCodigo = findViewById(R.id.txtCodigo);
        lbltel2 = findViewById(R.id.lbltel2);
        txtNombre = findViewById(R.id.txtNombre);
        lblMarca = findViewById(R.id.lblMarca);
        txtMarca = findViewById(R.id.txtMarca);
        lblNota = findViewById(R.id.lblNota);
        txtPrecio = findViewById(R.id.txtPrecio);
        lblradiog = findViewById(R.id.lblradiog);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnEditar = findViewById(R.id.btnEditar);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnBuscar = findViewById(R.id.btnBuscar);
        db = new AgendaProductos(this);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = Long.parseLong(txtCodigo.getText().toString());
                db.openDataBase();
                Productos productos = db.getContacto(id);
                if(productos == null)
                {
                    Toast.makeText(getApplicationContext(), "No se encontro producto", Toast.LENGTH_LONG).show();

                }
                else
                {
                    saveProducto = productos;
                    txtMarca.setText(saveProducto.getMarca());
                    txtNombre.setText(saveProducto.getNombre());
                    txtPrecio.setText(saveProducto.getPrecio());
                    lblradiog.check(saveProducto.getCaducidad().equals("Perecedero") ? R.id.r2 : R.id.r1);
                }
                db.cerrar();
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                if(txtNombre.getText().toString().equals("")||
                        txtCodigo.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Inserte código y nombre de Producto",
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Productos nProductos = new Productos();
                    nProductos.setNombre(txtNombre.getText().toString());
                    nProductos.setPrecio(txtPrecio.getText().toString());
                    nProductos.setMarca(txtMarca.getText().toString());
                    db.UpdateProducto(nProductos, Long.parseLong(saveProducto.getCodigo()));
                    Toast.makeText(getApplicationContext(), "Se actualizo el producto",
                            Toast.LENGTH_SHORT).show();
                }
                db.cerrar();
                limpiar();
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = Long.parseLong(txtCodigo.getText().toString());
                db.openDataBase();
                db.deleteProducto(id);
                db.cerrar();
                Toast.makeText(getApplicationContext(), "Se elimino el producto", Toast.LENGTH_LONG).show();
                limpiar();
            }
        });



    }

    private void limpiar()
    {
        saveProducto = null;
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtMarca.setText("");
        txtNombre.setText("");
    }


}
